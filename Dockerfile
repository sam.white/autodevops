FROM python:alpine

RUN echo 'https://dl-cdn.alpinelinux.org/alpine/v3.9/main' >> /etc/apk/repositories

RUN apk update

RUN apk add --no-cache mariadb=10.3.25-r0 
